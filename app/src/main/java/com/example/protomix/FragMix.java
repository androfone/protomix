package com.example.protomix;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class FragMix extends Fragment {

    MainViewModel mViewModel;
    MixerControlView mixer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        return inflater.inflate(R.layout.frag_mix, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mixer = view.findViewById(R.id.mixer);
//        MixerControlView.setUpWithViewModel(mViewModel, getViewLifecycleOwner(), mixer);
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
//        PanerControlView.setUpWithViewModel(mViewModel, paner);
//    }

    @Override
    public void onResume() {
        super.onResume();
        MixerControlView.setUpWithViewModel(mViewModel, getViewLifecycleOwner(), mixer);
    }
}
