package com.example.protomix;

import android.graphics.Point;
import android.graphics.PointF;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainViewModel extends ViewModel {

    private static final String DEBUG_TAG = "MainViewModel";
//    public static Object Track;

    private final ArrayList<Track> tracks = new ArrayList<>();
    private MutableLiveData<ArrayList<Track>> alltracks = new MutableLiveData<>();

    public MainViewModel() {
        super();
        for (int i=0; i<3; i++) {
            tracks.add(new Track(Integer.toString(i)));
        }
        alltracks.setValue(tracks);
    }


    public Track getTrack(int index) {
        return Objects.requireNonNull(alltracks.getValue()).get(index);
    }

//    public LiveData<Track> getTrack(int index) {
//        return tracks.getValue();
//    }

    public Track getTrack(String tag) {
        if (!tracks.isEmpty()) {
            for (Track t : tracks) {
                if (t.mTag.equals(tag)) {
                    Log.d(DEBUG_TAG, "Track with tag \"" + tag + "\" found");
                    return t;
                }
            }
            Track t = new Track(tag);
            Log.d(DEBUG_TAG, "Track not found, creating new Track with tag \"" + tag + "\"");
            tracks.add(t);
            return t;
        }
        else {
            Track t = new Track(tag);
            Log.d(DEBUG_TAG, "Track not found, creating new Track with tag \"" + tag + "\"");
            tracks.add(t);
            return t;
        }
    }

    public static class Track {
        String mTag;

        private MutableLiveData<PointF> mPanNormPosition = new MutableLiveData<>();

        Track(String tag) {
            mTag = tag;
        }

        public LiveData<PointF> getPanNormPosition() {
            return mPanNormPosition;
        }

        public void setPanNormPosition(PointF p) {
            this.mPanNormPosition.setValue(p);
        }
    }
}
