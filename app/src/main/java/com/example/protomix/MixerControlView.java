package com.example.protomix;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RemoteViews.RemoteView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.customview.widget.Openable;
import androidx.lifecycle.LifecycleOwner;

import java.util.ArrayList;

@RemoteView
public class MixerControlView extends FrameLayout implements Openable {//}, View.OnTouchListener {
    private static final String DEBUG_TAG = "MixerControlView";

    private int mClosedWidth;
    private int mOpenedWidth;
    private int mClosedHeight;
    private int mOpenedHeight;
    private int vWidth;
    private int vHeight;
    private float mHeightRatio;
    private boolean isOpen = false;


    private Paint mStrokePaint, mFillPaint, mInsetPaint;
    private Path mMarker;
    private Path mPath;
    private Path mExpandIndicator;
    private Region hotSpot;
    private Drawable mBackground;

    private int width;
    private int height;

    private DisplayMetrics mDisplayMetrics;
    private int mScaledUnit;

    private int offset;

    private MainViewModel mViewModel;
    private LifecycleOwner mOwner;

    private MixerCursorView2 mCursor1;
    private MixerCursorView2 mCursor2;
    private MixerCursorView2 mCursor3;

    private ArrayList<View> mChildList;



    public MixerControlView(Context context) {
        super(context);
        init(context, null);
    }

    public MixerControlView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    // Convenience constructor
    public MixerControlView(Context context, MainViewModel viewModel) {
        super(context);
//        mViewModel = viewModel;
        init(context, null);
    }

    public static void setUpWithViewModel(MainViewModel viewModel,
                                          @NonNull androidx.lifecycle.LifecycleOwner owner,
                                          @NonNull MixerControlView mixer) {
        mixer.mViewModel = viewModel;
        mixer.mOwner = owner;
//        mixer.setUpWithViewModelInternal(viewModel, owner);
    }

    private void setUpWithViewModelInternal(MainViewModel viewModel, LifecycleOwner owner) {
        Log.d("YES Where I Want To Be", "setUpWithViewModelInternal");
        mViewModel = (MainViewModel)viewModel;
        mViewModel.getTrack(0).getPanNormPosition().observe(owner, point -> {
            mCursor1.setNormPosition(point);
            Log.d(DEBUG_TAG, "mCursor1.repositionCursor(point.x, point.y) x " + point.x + " y " + point.y);
        });
        mViewModel.getTrack(1).getPanNormPosition().observe(owner,
                point -> mCursor2.setNormPosition(point));
        mViewModel.getTrack(2).getPanNormPosition().observe(owner,
                point -> mCursor3.setNormPosition(point));

        mCursor1.setMixerCursorListener(pf -> mViewModel.getTrack(0).setPanNormPosition(pf));

        mCursor2.setMixerCursorListener(pf -> mViewModel.getTrack(1).setPanNormPosition(pf));

        mCursor3.setMixerCursorListener(pf -> mViewModel.getTrack(2).setPanNormPosition(pf));
    }


    protected void init(Context context, AttributeSet attrs) {

        int mStrokeColor = ContextCompat.getColor(context, R.color.default_mixer_control_color);
        int mFillColor = ContextCompat.getColor(context, R.color.default_mixer_control_background);

        float border = getResources().getDimensionPixelSize(R.dimen.border_size);
        float inset = getResources().getDimensionPixelSize(R.dimen.cursor_size);

        mDisplayMetrics = getResources().getDisplayMetrics();

        mStrokePaint = new Paint();
        mStrokePaint.setColor(mStrokeColor);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeJoin(Paint.Join.ROUND);
        mStrokePaint.setStrokeWidth(border);
        mStrokePaint.setAntiAlias(true);

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setAntiAlias(true);
        mFillPaint.setColor(mFillColor);

        mInsetPaint = new Paint();
        mInsetPaint.setStyle(Paint.Style.STROKE);
        mInsetPaint.setStrokeWidth(inset);
        mInsetPaint.setAntiAlias(true);
        mInsetPaint.setColor(mStrokeColor);

        hotSpot = new Region();

        mBackground = getResources().getDrawable(R.drawable.lin_grad);

//        mBackground = new MixerBackground(context);
//        addView(mBackground);
        int halo1 = getResources().getColor(R.color.cursor_halo_color_1);
        int halo2 = getResources().getColor(R.color.cursor_halo_color_2);
        int halo3 = getResources().getColor(R.color.cursor_halo_color_3);
        mCursor1 = new MixerCursorView2(context, halo1);
        addView(mCursor1);
        mCursor2 = new MixerCursorView2(context, halo2);
        addView(mCursor2);
        mCursor3 = new MixerCursorView2(context, halo3);
        addView(mCursor3);


        setWillNotDraw(false);

        int unit = getResources().getInteger(R.integer.unit);
        int w = getResources().getInteger(R.integer.filter_grid_width) * unit;
        int h = getResources().getInteger(R.integer.loop_grid_height) * unit;
        int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, mDisplayMetrics);
        int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, mDisplayMetrics);
        mScaledUnit = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unit, mDisplayMetrics);
        vWidth = mClosedWidth = width;
        vHeight = mClosedHeight = height;
        mHeightRatio = (float)mClosedHeight / mClosedWidth;
        mOpenedWidth = mDisplayMetrics.widthPixels;
        mOpenedHeight = (int) (mOpenedWidth * mHeightRatio);
 //       createView();
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int unit = getResources().getInteger(R.integer.unit);
        int w = (int) (getResources().getInteger(R.integer.filter_grid_width) * unit);
        int h = (int) (getResources().getInteger(R.integer.filter_grid_height) * unit);
//        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, mDisplayMetrics);
//        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, mDisplayMetrics);
        mScaledUnit = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unit, mDisplayMetrics);
        setMeasuredDimension(vWidth, vHeight);
//        setMeasuredDimension(width, height);
//        setMeasuredDimension(isOpen() ? mOpenedWidth : mClosedWidth,
//                            isOpen() ? mOpenedHeight : mClosedHeight);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        width = getMeasuredWidth();
        height = getMeasuredHeight();
        createView();
        setUpWithViewModelInternal(mViewModel, mOwner);
    }


    @Override
    protected void onDraw(Canvas canvas) {

        mBackground.setBounds(offset, offset, width-offset, height-offset);
        mBackground.draw(canvas);
//        canvas.drawPath(mPath, mFillPaint);
        canvas.drawPath(mPath, mStrokePaint);
//        canvas.drawPath(mMarker, mStrokePaint);
        canvas.drawPath(mExpandIndicator, mInsetPaint);

        super.onDraw(canvas);
    }

    // Needed to draw the marker after all the childs
    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        canvas.drawPath(mMarker, mStrokePaint);
    }

    private void createView() {
        if (width <= 0 || height <= 0)
            return;

//        offset = 16; //32;
        offset = mScaledUnit / 2;
        float centerX = (float) width / 2;
        mPath = new Path();

        // create border and background path
        RectF r = new RectF(offset, offset, width - offset, height - offset);
        mPath.addRect(r, Path.Direction.CCW);
        mPath.close();

        // create marker path
        mMarker = new Path();
        mMarker.moveTo(centerX, height- offset);
        mMarker.lineTo(centerX, height-(offset *2));

        // create expand indicator inset
        float inset = offset / 2;
        mExpandIndicator = new Path();
        mExpandIndicator.moveTo(offset+inset, offset+offset);
        mExpandIndicator.lineTo(offset+inset, offset+inset);
        mExpandIndicator.lineTo(offset+offset, offset+inset);
        hotSpot.set(offset, offset, offset+offset, offset+offset);

        invalidate();
    }

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public void open() {
        ValueAnimator anim = ValueAnimator.ofInt(mClosedWidth, mOpenedWidth);

        anim.addUpdateListener((ValueAnimator.AnimatorUpdateListener) valueAnimator -> {
            Log.d(DEBUG_TAG, "open(): valueAnimator: " + (Integer) valueAnimator.getAnimatedValue());

            vWidth = (Integer) valueAnimator.getAnimatedValue();
            vHeight = (int) (vWidth * mHeightRatio);
            getLayoutParams().width = vWidth;
            getLayoutParams().height = vHeight;
            requestLayout();
        });
        anim.setDuration(222);
        anim.start();
        isOpen = true;
    }

    @Override
    public void close() {
        ValueAnimator anim = ValueAnimator.ofInt(mOpenedWidth, mClosedWidth);
        anim.addUpdateListener((ValueAnimator.AnimatorUpdateListener) valueAnimator -> {
            Log.d(DEBUG_TAG, "open(): valueAnimator: " + (Integer) valueAnimator.getAnimatedValue());
            vWidth = (Integer) valueAnimator.getAnimatedValue();
            vHeight = (int) (vWidth * mHeightRatio);
            getLayoutParams().width = vWidth;
            getLayoutParams().height = vHeight;
            requestLayout();
        });
        anim.setDuration(222);
        anim.start();
        isOpen = false;
    }

//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                float x = event.getX();
//                float y = event.getY();
//                if (hotSpot.contains((int) x, (int) y)) {
//                    Log.d(DEBUG_TAG, "!!!   !!!   !!!!   HOT SPOT TOUCH!   !!!   !!!!   !!!!");
//                    if (isOpen) close();
//                    else open();
//                }
//                break;
//            case MotionEvent.ACTION_UP:
//                break;
//            case MotionEvent.ACTION_MOVE:
//                break;
//        }
//        return false;
//    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                float x = event.getX();
                float y = event.getY();
                if (hotSpot.contains((int) x, (int) y)) {
                    Log.d(DEBUG_TAG, "!!!   !!!   !!!!   HOT SPOT TOUCH!   !!!   !!!!   !!!!");
                    if (isOpen) close();
                    else open();
                } //else dispatchGenericMotionEvent(event);
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }
        return true;
    }

}
