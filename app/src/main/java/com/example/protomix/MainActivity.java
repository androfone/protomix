package com.example.protomix;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.graphics.PointF;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "MainActivity";

    private static final String CUR_FRAG_KEY = "curFrag";
    private static final String CUR_TAB_KEY = "curTab";

    // Static variables for the SelectorTabLayout popupmenu listener switch since Google will
    // prevent the use of non final variables in switch statement in future version (5) of gradle.
    // If it cause problems, convert the switch to cascading if's statement
    // and use the id's from the menu item directly instead of those.

    SelectorTabLayout mSessionTab;


    private FragmentManager mFragmentManager;
    private String[] mTags = new String[]{"MF1", "MF2", "MF3"}; // Hard coded session fragment tag
    private TabLayout mTab;

    public MainViewModel mMainViewModel;


    private PanerControlView Pan1;
    private PanerControlView Pan2;
    private PanerControlView Pan3;
    private MixerCursorView cur1;
    private MixerCursorView cur2;
    private MixerCursorView cur3;
    private MixerControlView mixer;

    private MediaPlayer player1;
    private MediaPlayer player2;
    private MediaPlayer player3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();

        mSessionTab = findViewById(R.id.session_tab_layout);
        mSessionTab.setOnSelectorTabActionListener(new SelectorTabLayout.OnSelectorTabActionListener() {
            @Override
            public void onTabSelected(int pos) {
                Log.e(DEBUG_TAG,"onTabSelected(mPosition): " + pos);
                if (!((SelectorTabLayout.SampleTab) mSessionTab.getChildAt(pos)).isLoaded()) {
                    Log.e(DEBUG_TAG,"tab is not loaded, call dialog ");
                    mSessionTab.loadSample("new sample");
                } else {
                    Log.e(DEBUG_TAG,"tab is loaded, just switch fragment ");
                }
                String tag = mTags[pos];
                Log.e("MainActivity", "tag: " + tag);

            }

            @Override
            public void onTabPopupItemSelected(int curTab, MenuItem item) {
            }

            @Override
            public void onTabPlayButtonClicked(int pos, boolean isPlaying) {
                if (pos == 0) {
                    if (isPlaying) {
                        play1();
                    } else {
                        stop1();
                    }
                }
                if (pos == 1) {
                    if (isPlaying) {
                        play2();
                    } else {
                        stop2();
                    }
                }
                if (pos == 2) {
                    if (isPlaying) {
                        play3();
                    } else {
                        stop3();
                    }
                }
            }
        });


        mMainViewModel = new ViewModelProvider(this).get(MainViewModel.class);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        mMainViewModel.getTrack(0).getPanNormPosition().observe(this, pf -> {
            Log.d(DEBUG_TAG,"observe.track1.getPanNormPosition() p.x: " + pf.x + " p.y: " + pf.y);
            if(player1 != null) {
                setPanVol1(pf.x, pf.y);
            }
        });

        mMainViewModel.getTrack(1).getPanNormPosition().observe(this, pf -> {
            Log.d(DEBUG_TAG,"observe.track2.getPanNormPosition() p.x: " + pf.x + " p.y: " + pf.y);
            if(player2 != null) {
                setPanVol2(pf.x, pf.y);
            }
        });

        mMainViewModel.getTrack(2).getPanNormPosition().observe(this, pf -> {
            Log.d(DEBUG_TAG,"observe.track3.getPanNormPosition() p.x: " + pf.x + " p.y: " + pf.y);
            if(player3 != null) {
                setPanVol3(pf.x, pf.y);
            }
        });

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    public void setPanVol1(float pan, float vol) {
        Log.d(DEBUG_TAG,"setPanVol1 pan: " + pan + " vol: " + vol);
        float l = (float) Math.sqrt((1-pan) * vol);
        float r = (float) Math.sqrt(pan * vol);
        Log.d(DEBUG_TAG,"setPanVol1 l: " + l + " r: " + r);
        player1.setVolume(l, r);
    }
    public void setPanVol2(float pan, float vol) {
        float l = (float) Math.sqrt((1-pan) * vol);
        float r = (float) Math.sqrt(pan * vol);
        player2.setVolume(l, r);
    }
    public void setPanVol3(float pan, float vol) {
        float l = (float) Math.sqrt((1-pan) * vol);
        float r = (float) Math.sqrt(pan * vol);
        player3.setVolume(l, r);
    }

    public void play1() {
        if (player1 == null) {
            player1 = MediaPlayer.create(this, R.raw.rainettes);
            player1.setLooping(true);
            player1.setOnCompletionListener(mp -> stopPlayer1());
        }
        player1.start();
    }

    public void stop1() {
        stopPlayer1();
    }
    private void stopPlayer1() {
        if (player1 != null) {
            player1.release();
            player1 = null;
        }
    }
    public void play2() {
        if (player2 == null) {
            player2 = MediaPlayer.create(this, R.raw.harmonique);
            player2.setLooping(true);
            player2.setOnCompletionListener(mp -> stopPlayer1());
        }
        player2.start();
    }

    public void stop2() {
        stopPlayer2();
    }
    private void stopPlayer2() {
        if (player2 != null) {
            player2.release();
            player2 = null;
        }
    }
    public void play3() {
        if (player3 == null) {
            player3 = MediaPlayer.create(this, R.raw.oiseau_nuit);
            player3.setLooping(true);
            player3.setOnCompletionListener(mp -> stopPlayer1());
        }
        player3.start();
    }

    public void stop3() {
        stopPlayer3();
    }
    private void stopPlayer3() {
        if (player3 != null) {
            player3.release();
            player3 = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopPlayer1();
        stopPlayer2();
        stopPlayer3();

    }
}