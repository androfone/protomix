package com.example.protomix;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

public class PanerControlView extends Tool {
    private static final String DEBUG_TAG = "PannerControlView";


    public interface PannerControlListener {
        void onNormPositionChanged(PointF pf);
    }

    private Paint mStrokePaint, mFillPaint, mCursorPaint, mCursorFillPaint, mMarkerPaint, mMaskPaint;
    private Rect drawRect;
    private Picture mCachedView;
    private Bitmap mCachedViewBitmap;
    private Picture mCachedCursor;
    private Bitmap mCachedCursorBitmap;
    private Path mMarker;

    private Rect mClipRect;
    private Region mRegion;
    private Path cursorPath;
    private Path fillPath;
    private float mDensity;
    private int width;
    private int height;

    private int offset;
    private int mCursorOffset;
    private int mCursorOffsetY;
    private int cursorHeight;
    private int cursorWidth;
    private int xRange;
    private int yRange;
    private int trueXRange;
    private int trueYRange;
    private int yMin;
    private int yMax;
    private float mCursorHalfWidth;
    private float mCursorHalfHeight;
    private float mCursorCornerRadius;
    private DisplayMetrics mDisplayMetrics;
    private int mScaledUnit;
    private boolean isInteractionEnabled;

    private MainViewModel mViewModel;
    private LifecycleOwner mOwner;
    private LiveData<Point> mPoint;
    private int track;

    private float mLastX;
    private float mLastY;


    private float centerX;
    private float centerY;
    private PanerControlView.PannerControlListener mListener;

    public PanerControlView(Context context) {
        super(context);
        Log.d(DEBUG_TAG+this.getId(), "CONSTRUCTOR");
        init(context, null);
    }

    public PanerControlView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Log.d(DEBUG_TAG+this.getId(), "CONSTRUCTOR from xml");
        init(context, attrs);
    }

    // Convenience constructor
    public PanerControlView(Context context, ViewModel viewModel) {
        super(context);
//        mViewModel = viewModel;
        init(context, null);
    }

    // Convenience constructor
    public PanerControlView(Context context, ViewModel viewModel, int track) {
        super(context);
        mViewModel = (MainViewModel)viewModel;
        this.track = track;
        init(context, null);
    }

    public void setTrack(int track) {
        this.track = track;
    }

    public void setUpVM(ViewModel viewModel, LifecycleOwner owner) {
        Log.d(DEBUG_TAG+this.getId(),"setUpVM(ViewModel viewModel, LifecycleOwner owner)");
        mViewModel = (MainViewModel) viewModel;
        mOwner = owner;
//        int id = this.getId();
//        mViewModel.getTrack(track).getPanNormPosition().observe(owner, new Observer<PointF>() {
//            @Override
//            public void onChanged(PointF pf) {
//                Log.d(DEBUG_TAG+id,"observe.track"+(track+1)+".setNormPosition(pf) p.x: " + pf.x + " p.y: " + pf.y);
//                setNormPosition(pf);
//            }
//        });
    }

    @Override
    protected void setUpWithViewModelInternal(ViewModel viewModel) {
        Log.d(DEBUG_TAG+this.getId()," setUpWithViewModelInternal");
        mViewModel = (MainViewModel) viewModel;
        int id = this.getId();
        mViewModel.getTrack(track).getPanNormPosition().observe(mOwner, new Observer<PointF>() {
            @Override
            public void onChanged(PointF pf) {
                Log.d(DEBUG_TAG+id,"observe.track"+(track+1)+".setNormPosition(pf) p.x: " + pf.x + " p.y: " + pf.y);
                setNormPosition(pf);
            }
        });

//
//        mViewModel.getEditMode().observe((LifecycleOwner) getContext(), aBoolean -> {
//            if (aBoolean) {
//                Log.e("FILTER", "mViewModel.getEditMode() aBoolean should be true: " + aBoolean);
//                setBackgroundColor(getResources().getColor(R.color.colorgray));
//                setAlpha(0.6f);
//                isInteractionEnabled = false; // User do not interact with the view, drag only
//            }
//            else {
//                Log.e("FILTER", "mViewModel.getEditMode() aBoolean should be false: " + aBoolean);
//                setBackground(null);
//                setAlpha(1);
//                isInteractionEnabled = true; // User interact with the view, no drag
//            }
//        });
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void init(Context context, AttributeSet attrs) {

        Log.d(DEBUG_TAG+this.getId(), " track"+(track+1) + " init()");
        int mStrokeColor = ContextCompat.getColor(context, R.color.default_paner_control_color);
        int mFillColor = ContextCompat.getColor(context, R.color.default_paner_control_background);
        int mCursorColor = ContextCompat.getColor(context, R.color.default_selection);
        // debug mask color
        int mMaskColor = ContextCompat.getColor(context, R.color.default_mask);

        float border = getResources().getDimensionPixelSize(R.dimen.border_size);
        float cursor = getResources().getDimensionPixelSize(R.dimen.cursor_size);
        float medium = getResources().getDimensionPixelSize(R.dimen.medium_size);
        float small = getResources().getDimensionPixelSize(R.dimen.small_size);
        float thin = getResources().getDimensionPixelSize(R.dimen.thin_size);
        float hairline = getResources().getDimensionPixelSize(R.dimen.hairline_size);
        float six = 6, four = 4, three = 3, two = 2, one = 1, half = 0.5f;

//        mCursorHalfHeight = getResources().getDimension()
        mDisplayMetrics = getResources().getDisplayMetrics();
        mCursorHalfHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, mDisplayMetrics);
        mCursorCornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4.5f, mDisplayMetrics);


        mStrokePaint = new Paint();
        mStrokePaint.setStrokeCap(Paint.Cap.ROUND);
        mStrokePaint.setColor(mStrokeColor);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeJoin(Paint.Join.ROUND);
        mStrokePaint.setStrokeWidth(border);
        mStrokePaint.setAntiAlias(true);

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setAntiAlias(true);
        mFillPaint.setColor(mFillColor);

        mCursorPaint = new Paint();
        mCursorPaint.setStyle(Paint.Style.STROKE);
        mCursorPaint.setStrokeWidth(cursor);
        mCursorPaint.setStrokeJoin(Paint.Join.ROUND);
        mCursorPaint.setAntiAlias(true);
        mCursorPaint.setColor(mCursorColor);


        mCursorFillPaint = new Paint();
        mCursorFillPaint.setStyle(Paint.Style.FILL);
        mCursorFillPaint.setAntiAlias(true);
        mCursorFillPaint.setColor(mStrokeColor);

        mMarkerPaint = new Paint();
        mMarkerPaint.setColor(mStrokeColor);
        mMarkerPaint.setStyle(Paint.Style.STROKE);
        mMarkerPaint.setStrokeWidth(border);
        mMarkerPaint.setAntiAlias(true);


        mMaskPaint = new Paint();
        mMaskPaint.setStyle(Paint.Style.FILL);
        mMaskPaint.setColor(mMaskColor);

        isInteractionEnabled = true;
    }

    public void setListener(PannerControlListener listener) {
        Log.d(DEBUG_TAG+this.getId(), " track"+(track+1) + " setListener(PannerControlListener listener)");
        mListener = listener;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(DEBUG_TAG+this.getId(), " track"+(track+1) + " onMeasure(int widthMeasureSpec, int heightMeasureSpec)");
        int unit = getResources().getInteger(R.integer.unit);
        int w = (int) (getResources().getInteger(R.integer.filter_grid_width) * unit);
        int h = (int) (getResources().getInteger(R.integer.filter_grid_height) * unit);
        int width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, mDisplayMetrics);
        int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, mDisplayMetrics);
        mScaledUnit = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unit, mDisplayMetrics);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        Log.d(DEBUG_TAG+this.getId(), " track"+(track+1) + " onSizeChanged(int w, int h, int oldw, int oldh)");
        super.onSizeChanged(w, h, oldw, oldh);

        width = getMeasuredWidth();
        height = getMeasuredHeight();
        drawRect = new Rect(0, 0, width, height);
        mRegion = new Region();
        createView();
        createCursor();

        if (mViewModel != null) {
            setUpWithViewModelInternal(mViewModel);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isInteractionEnabled) {

            super.onTouchEvent(event);
//            Log.d(DEBUG_TAG, "!!!   !!!   !!!!   TOUCH!   !!!   !!!!   !!!!");
            float xmin = offset;
            float xmax = width - offset;
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    x = Math.min(xmax, Math.max(xmin, x));
                    y = Math.min(yMax, Math.max(yMin, y));

                    float factor = ((y - yMin) / yRange);
                    // range = (halfrange-halfcursor) + halfcursor
 //                   mCursorHalfWidth = (factor * ((xRange / 2) - (offset / 2))) + (offset / 2);


 //                   float cutoff = (x - offset) / xRange;
                    float cutoff = (x - offset) / ((xRange*2)-offset);
                    Log.d(DEBUG_TAG+this.getId(), " track"+(track+1) + " factor " + factor + " cutoff " + cutoff);

                    mLastX = x;
                    mLastY = y;

                    repositionCursor((int) x, (int) y);

                    float pan = (x - offset) / ((xRange*2)-offset);;
                    float vol = (y-yMin)/(float)yRange;

                    if (mListener != null) {
                        mListener.onNormPositionChanged(new PointF(pan, vol));
                    }

                    if (mViewModel != null) {
                        mViewModel.getTrack(track).setPanNormPosition(new PointF(pan, vol));
                    }

                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
        }
        return isInteractionEnabled;
    }

    public void setNormPosition(PointF pf) {
        Log.d(DEBUG_TAG, " track"+(track+1) + " setNormPosition(PointF pf) " + pf.x + " " + pf.y);
        int x = (int) ((pf.x * ((xRange*2)-offset)) + offset);
        int y = (int) ((pf.y * yRange) + yMin);
        mCursorHalfWidth = (pf.y * ((xRange/2)-(offset/2))) + (offset/2);
        positionCursor(x, y);
    }

    public void repositionCursor(int x, int y) {
        float factor = (((float)y - yMin) / yRange);
//        float pan = (x - offset) / ((xRange*2)-offset);

        Log.d(DEBUG_TAG+this.getId(), " track"+(track+1) + " repositionCursor(int x, int y) y, factor: " + y +" "+ factor);
        // range = (halfrange-halfcursor) + halfcursor
        mCursorHalfWidth = (factor * ((xRange/2)-(offset/2))) + (offset/2);
        Log.d(DEBUG_TAG, " track"+(track+1) + " repositionCursor(int x, int y) mCursorHalfWidth: " + mCursorHalfWidth);
        positionCursor(x, y);
    }

    void positionCursor(int x, int y) {
        Canvas canvas;
        Log.d(DEBUG_TAG + this.getId(), " track"+(track+1) + " positionCursor(int x, int y): " + x + " " + y);
        if (mCachedCursor != null) {
//            Log.d(DEBUG_TAG + this.getId(), "positionCursor mCachedCursor: " + mCachedCursor);
//        Log.d(DEBUG_TAG, "positionCursor mCachedCursorBitmap: " + mCachedCursorBitmap);
            canvas = mCachedCursor.beginRecording(width, height);
            cursorPath.reset();
//        cursorPath.addRoundRect(new RectF(-halfWidth, -16, halfWidth, 16),
//                10.0f, 10.0f, Path.Direction.CCW );
            cursorPath.addRoundRect(new RectF(-mCursorHalfWidth, -mCursorHalfHeight, mCursorHalfWidth, mCursorHalfHeight),
                    10.0f, 10.0f, Path.Direction.CCW);

            cursorPath.close();
            canvas.translate(x, y);
            canvas.drawPath(cursorPath, mCursorFillPaint);
            canvas.drawPath(cursorPath, mCursorPaint);

            if (mCachedCursor != null)
                mCachedCursor.endRecording();
            invalidate();
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if ((mCachedView != null) && (mCachedCursor != null)) {
            canvas.drawPicture(mCachedView);
            canvas.clipRect(mClipRect);
            canvas.drawPicture(mCachedCursor);
            canvas.drawPath(mMarker, mMarkerPaint);
        } else if (mCachedViewBitmap != null && mCachedCursorBitmap != null) {
            canvas.drawBitmap(mCachedViewBitmap, null, drawRect, null);
            canvas.drawBitmap(mCachedCursorBitmap, null, drawRect, null);
        }
    }

    private void createView() {
        if (width <= 0 || height <= 0)
            return;
        Log.d(DEBUG_TAG+this.getId(), " track"+(track+1) + " createView() width, height " + width + " " + height);

        Canvas cacheCanvas;
        if (Build.VERSION.SDK_INT >= 23 && isHardwareAccelerated()) {
            Log.d(DEBUG_TAG+this.getId(), "Build.VERSION.SDK_INT >= 23");
            mCachedView = new Picture();
            cacheCanvas = mCachedView.beginRecording(width, height);
        } else {
            mCachedViewBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            cacheCanvas = new Canvas(mCachedViewBitmap);
        }

        float borderdim = getResources().getDimensionPixelSize(R.dimen.border_size);
        float cursordim = getResources().getDimensionPixelSize(R.dimen.cursor_size);
//        offset = 16; //32;
        offset = mScaledUnit/2;
        centerX = width / 2;
        centerY = height / 2;
        int margin = (offset/2) + (int)borderdim; //12; // 16 + strokewidth
        int xmin = offset;
        int xmax = width - offset;
        xRange = xmax / 2; //xmax - xmin;
        yMin = offset + (int)cursordim; // offset + "cursor_size" stroke width (4);
        yMax = height - yMin;
        yRange = yMax - yMin;

        // x inside range minus cursor min width
        mCursorHalfWidth = (xRange/ 2) - (offset*2); // will be scaled by y 0 - 1

        Path path = new Path();
        mClipRect = new Rect(margin, margin, width-margin, height-margin);

        // create border and background path
        RectF r = new RectF(offset, offset, width - offset, height - offset);
        path.addRect(r, Path.Direction.CCW);
        path.close();

        //       cacheCanvas.drawColor(mMaskPaint.getColor());
        cacheCanvas.drawPath(path, mStrokePaint);
        cacheCanvas.drawPath(path, mFillPaint);


        // create marker
        mMarker = new Path();
        mMarker.moveTo(centerX, height-offset);
        mMarker.lineTo(centerX, height-(offset*2));

        if (mCachedView != null)
            mCachedView.endRecording();
        invalidate();
    }

    private void createCursor() {
        if (width <= 0 || height <= 0)
            return;

        Log.d(DEBUG_TAG+this.getId(), " track"+(track+1) + " createCursor()");
        Canvas cacheCanvas;
        if (Build.VERSION.SDK_INT >= 23 && isHardwareAccelerated()) {
            mCachedCursor = new Picture();
            cacheCanvas = mCachedCursor.beginRecording(width, height);
        } else {
            mCachedCursorBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            cacheCanvas = new Canvas(mCachedCursorBitmap);
        }

        float cursorHeight = mCursorHalfHeight * 2;
        Path path = new Path();
        // initial position full width bottom - position is position - x center
        RectF r = new RectF(-(xRange/2), yMax-cursorHeight, (xRange/2), yMax);
        path.addRoundRect( r, 10.0f, 10.0f, Path.Direction.CCW);
        path.close();

        cacheCanvas.translate(centerX, 0);
        cacheCanvas.drawPath(path, mCursorFillPaint);
        cacheCanvas.drawPath(path, mCursorPaint);
        cursorPath = path;

        if (mCachedCursor != null)
            mCachedCursor.endRecording();
        invalidate();
    }

}
