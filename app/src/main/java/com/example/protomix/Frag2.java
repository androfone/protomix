package com.example.protomix;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class Frag2 extends Fragment {

    MainViewModel mViewModel;
    PanerControlView paner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        return inflater.inflate(R.layout.frag_2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        paner = view.findViewById(R.id.pan_2);
        paner.setTrack(1);
//        mViewModel.getTrack(1).getPanNormPosition().observe(getViewLifecycleOwner(),
//                pointF -> paner.setNormPosition(pointF));
//        paner.setListener(pf -> mViewModel.getTrack(1).setPanNormPosition(pf));
        paner.setUpVM(mViewModel, getViewLifecycleOwner());
//        PanerControlView.setUpWithViewModel(mViewModel, paner);
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
//        PanerControlView.setUpWithViewModel(mViewModel, paner);
//    }

    @Override
    public void onResume() {
        super.onResume();
//        paner.setUpVM(mViewModel, getViewLifecycleOwner());
//        PanerControlView.setUpWithViewModel(mViewModel, paner);
    }
}
