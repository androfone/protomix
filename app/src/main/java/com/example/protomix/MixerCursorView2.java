package com.example.protomix;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.view.MotionEventCompat;

public class MixerCursorView2  extends View {
    private static final String DEBUG_TAG = "MixerCursorView";


    public interface MixerCursorListener {
        void onNormPositionChanged(PointF pf);
    }
    private MixerCursorView.MixerCursorListener mListener;

    public void setMixerCursorListener(MixerCursorView.MixerCursorListener listener) {
        mListener = listener;
    }

    private Paint mCursorPaint, mCursorFillPaint, mHaloPaint;
    private Shader mHaloShader;
    private Rect drawRect;
    private Picture mCachedCursor;
    private Bitmap mCachedCursorBitmap;
    private Path mMarker;
    Canvas haloCanvas;
    private int mHaloColor;

    private Rect mCursorClipRect;
    private Rect mClipRect;
    private Region mRegion;
    private Path cursorPath;
    private Path cursorHaloPath;
    private float borderSize;
    private float cursorSize;
    private float mDensity;
    private int width;
    private int height;


    private int offset;
    private int mCursorOffset;
    private int mCursorOffsetY;

    private int xRange;
    private int yRange;
    private int yMin;
    private int yMax;
    private float mCursorHalfWidth;
    private float mCursorHalfHeight;
    private float mCursorCornerRadius;
    private DisplayMetrics mDisplayMetrics;
    private int mScaledUnit;
    private boolean isInteractionEnabled;

    private float mLastX;
    private float mLastY;
    private PointF mLastPoint;
    private int mCurX;
    private int mCurY;


    private float centerX;
    private float centerY;

    private Drawable halo;


    public MixerCursorView2(Context context, int color) {
        super(context);
        init(context, null);
        mHaloColor = color;
    }

    public MixerCursorView2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attrs) {

        Log.d(DEBUG_TAG, "INIT");
        int mStrokeColor = ContextCompat.getColor(context, R.color.default_mixer_control_color);
        int mCursorColor = ContextCompat.getColor(context, R.color.default_selection);

        float border = getResources().getDimensionPixelSize(R.dimen.border_size);
        float cursor = getResources().getDimensionPixelSize(R.dimen.cursor_size);
        float medium = getResources().getDimensionPixelSize(R.dimen.medium_size);
        float small = getResources().getDimensionPixelSize(R.dimen.small_size);
        float thin = getResources().getDimensionPixelSize(R.dimen.thin_size);
        float hairline = getResources().getDimensionPixelSize(R.dimen.hairline_size);
        float six = 6, four = 4, three = 3, two = 2, one = 1, half = 0.5f;

//        mCursorHalfHeight = getResources().getDimension()
        mDisplayMetrics = getResources().getDisplayMetrics();
        mCursorHalfHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, mDisplayMetrics);
        mCursorCornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4.5f, mDisplayMetrics);

        mCursorPaint = new Paint();
        mCursorPaint.setStyle(Paint.Style.STROKE);
        mCursorPaint.setStrokeWidth(cursor);
        mCursorPaint.setStrokeJoin(Paint.Join.ROUND);
        mCursorPaint.setAntiAlias(true);
        mCursorPaint.setColor(mCursorColor);

        int unit = getResources().getInteger(R.integer.unit);
        int w = (int) (getResources().getInteger(R.integer.filter_grid_width) * unit);
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, mDisplayMetrics);

        // Here we just need to NOT have a null shader; it will be reset in onSizeChange()
        mHaloShader = new RadialGradient(0, 0, 10f, mHaloColor, Color.TRANSPARENT, Shader.TileMode.CLAMP);
        mHaloPaint = new Paint();
        mHaloPaint.setStyle(Paint.Style.FILL);
        mHaloPaint.setStrokeWidth(cursor);

        mHaloPaint.setAntiAlias(true);
        mHaloPaint.setColor(mCursorColor);
//        mHaloPaint.setShader(mHaloShader);



        mCursorFillPaint = new Paint();
        mCursorFillPaint.setStyle(Paint.Style.FILL);
        mCursorFillPaint.setAntiAlias(true);
        mCursorFillPaint.setColor(mStrokeColor);

        isInteractionEnabled = true;
        mRegion = new Region();
        setClipToOutline(true);
        mLastPoint = new PointF(0.5f, 0.95f);
        cursorSize = cursor;

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int unit = getResources().getInteger(R.integer.unit);
        int w = (int) (getResources().getInteger(R.integer.filter_grid_width) * unit);
        int h = (int) (getResources().getInteger(R.integer.filter_grid_height) * unit);
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, mDisplayMetrics);
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, mDisplayMetrics);
        mScaledUnit = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, unit, mDisplayMetrics);

        Log.d(DEBUG_TAG, "!!!   !!!   !!!!   setMeasuredDimension(width, height)!   !!!   !!!!   !!!!");
        Log.d(DEBUG_TAG, "width " + width + " height " + height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d(DEBUG_TAG, "!!!   !!!   !!!!   onSizeChanged()!   !!!   !!!!   !!!!");

        width = getMeasuredWidth();
        height = getMeasuredHeight();
        drawRect = new Rect(0, 0, width, height);
        mRegion = new Region();
        Log.d(DEBUG_TAG, "width " + width + " height " + height);
        mHaloShader = new RadialGradient(0, 0, (float) (width/2), mHaloColor, Color.TRANSPARENT, Shader.TileMode.CLAMP);
        mHaloPaint.setShader(mHaloShader);
        createCursor();
        setNormPosition(mLastPoint);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isInteractionEnabled) {

//            super.onTouchEvent(event);
            Log.d(DEBUG_TAG, "!!!   !!!   !!!!   TOUCH!   !!!   !!!!   !!!!");
            float xmin = offset;
            float xmax = width - xmin;
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
//                    mCursorClipRect.offsetTo((int)(mLastX-mCursorHalfWidth), (int)(mLastY-mCursorHalfHeight));
                    if (!mCursorClipRect.contains((int)x, (int)y)) {
                        Log.d(DEBUG_TAG, "!!! !mCursorClipRect.contains((int)x, (int)y) !!!");
//                super.onTouchEvent(event);
                        return false;
                    }

                    break;
                case MotionEvent.ACTION_MOVE:
                    x = Math.min(xmax, Math.max(xmin, x));
                    y = Math.min(yMax, Math.max(yMin, y));

                    float factor = ((y - yMin) / yRange);
                    // range = (halfrange-halfcursor) + halfcursor
                    mCursorHalfWidth = (factor * ((xRange / 2) - (offset / 2))) + (offset / 2);


                    float cutoff = (x - offset) / xRange;
                    Log.d(DEBUG_TAG, "factor " + factor + " cutoff " + cutoff);

                    mLastX = x;
                    mLastY = y;

                    positionCursor((int) x, (int) y);

                    float pan = (x - offset) / ((xRange*2)-offset);
                    float vol = (y-yMin)/(float)yRange;

                    if (mListener != null) {
                        mListener.onNormPositionChanged(new PointF(pan, vol));
                    }

                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
        }
        return isInteractionEnabled;
    }

    public void setNormPosition(PointF pf) {
        int x = (int) ((pf.x * ((xRange*2)-offset)) + offset);
        int y = (int) ((pf.y * yRange) + yMin);
        mCursorHalfWidth = (pf.y * ((xRange)-(offset/2))) + (offset/2);
        mLastPoint = pf;
        Matrix mtx = new Matrix();
        float haloFactor = (pf.y * 0.95f) + 0.05f;
        mtx.setScale(haloFactor, 0.05f);
        mHaloShader.setLocalMatrix(mtx);
        mHaloPaint.setShader(mHaloShader);
        mHaloPaint.setAlpha((int)(haloFactor*255));
        positionCursor(x, y);
    }

    public void repositionCursor(int x, int y) {
        float factor = (((float) y - yMin) / yRange);
        Log.d(DEBUG_TAG, "repositionCursor(int x, int y) y, factor: " + y + " " + factor);
        // range = (halfrange-halfcursor) + halfcursor
        mCursorHalfWidth = (factor * ((xRange / 2) - (offset / 2))) + (offset / 2);
        Log.d(DEBUG_TAG, "repositionCursor(int x, int y) mCursorHalfWidth: " + mCursorHalfWidth);
        positionCursor(x, y);
    }

    void positionCursor(int x, int y) {
        Log.d(DEBUG_TAG, "positionCursor(int x, int y) x," + x + " y " + y);
        if (mCachedCursor != null) {
            Canvas canvas;
            canvas = mCachedCursor.beginRecording(width, height);
            cursorPath.reset();
            cursorPath.addCircle(0, 0, mCursorHalfHeight, Path.Direction.CCW);
            cursorPath.close();

            cursorHaloPath.reset();
            cursorHaloPath.addOval(-(mCursorHalfWidth), -mCursorHalfHeight, mCursorHalfWidth, mCursorHalfHeight, Path.Direction.CCW);
            cursorHaloPath.close();
            mCursorClipRect.set((int)-mCursorHalfHeight, (int)-mCursorHalfHeight, (int)mCursorHalfHeight, (int)mCursorHalfHeight);
            mCursorClipRect.offsetTo(x-(int)mCursorHalfHeight, y-(int)mCursorHalfHeight);
            mCursorClipRect.inset((int)-cursorSize, (int)-cursorSize);

            canvas.translate(x, y);

            canvas.drawPath(cursorHaloPath, mHaloPaint);
            canvas.drawPath(cursorPath, mCursorFillPaint);
            canvas.drawPath(cursorPath, mCursorPaint);


            if (mCachedCursor != null)
                mCachedCursor.endRecording();
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        if (mCachedCursor != null) {
            canvas.clipRect(mClipRect);
            canvas.drawPicture(mCachedCursor);

        } else if (mCachedCursorBitmap != null) {
            canvas.drawBitmap(mCachedCursorBitmap, null, drawRect, null);
        }

    }

    private void createCursor() {
        if (width <= 0 || height <= 0)
            return;

        MixerControlView mixer = (MixerControlView) getParent();
        int mixerheight = mixer.getHeight();
        height = mixerheight;
        Log.d(DEBUG_TAG, "!!!   !!!   !!!!   mixer.getHeight(): " + mixerheight + "  !!!   !!!!   !!!!");
        Log.d(DEBUG_TAG, "!!!   !!!   !!!!   createCursor()!   !!!   !!!!   !!!!");

        if (Build.VERSION.SDK_INT >= 23 && isHardwareAccelerated()) {
            mCachedCursor = new Picture();
        } else {
            mCachedCursorBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        }

        float borderdim = getResources().getDimensionPixelSize(R.dimen.border_size);
        float cursordim = getResources().getDimensionPixelSize(R.dimen.cursor_size);
//        offset = 16; //32;
        offset = mScaledUnit / 2;
        centerX = width / 2;
        int margin = (offset / 2) + (int) borderdim; //12; // 16 + strokewidth

        int xmax = width - offset;
        xRange = xmax / 2; //xmax - xmin;
        yMin = offset + (int) cursordim; // offset + "cursor_size" stroke width (4);
        yMax = height - yMin;
        yRange = yMax - yMin;

        // x inside range minus cursor min width
        mCursorHalfWidth = (xRange / 2) - (offset * 2); // will be scaled by y 0 - 1

        mClipRect = new Rect(margin, margin, width - margin, height - margin);

        float cursorHeight = mCursorHalfHeight * 2;

        cursorPath = new Path();
        cursorPath.addCircle(0, 0, mCursorHalfHeight, Path.Direction.CCW);
        cursorPath.close();
        cursorHaloPath = new Path();
//        cursorHaloPath.addOval(-(xRange / 2), -mCursorHalfHeight, (xRange / 2), mCursorHalfHeight, Path.Direction.CCW);
        cursorHaloPath.addOval(-(xRange / 2), -xRange / 2, (xRange / 2), xRange / 2, Path.Direction.CCW);
        cursorHaloPath.close();

        RectF r = new RectF(-mCursorHalfHeight, -mCursorHalfHeight, mCursorHalfHeight, mCursorHalfHeight);
        mCursorClipRect = new Rect((int)-cursorHeight, (int)-cursorHeight, (int)cursorHeight, (int)cursorHeight);
        mCursorClipRect.inset((int)-cursordim,(int)-cursordim);


        if (mCachedCursor != null)
            mCachedCursor.endRecording();
        invalidate();
    }

}
