package com.example.protomix;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;


public class SelectorTabLayout extends LinearLayoutCompat implements View.OnClickListener,
        PopupMenu.OnMenuItemClickListener {
//    private static final String DEBUG_TAG = "SelectorTabLayout";

    public interface OnSelectorTabActionListener {
        void onTabSelected(int tab);
        void onTabPopupItemSelected(int curTab, MenuItem item);
        void onTabPlayButtonClicked(int tabPos, boolean isPlaying);
    }
    private OnSelectorTabActionListener mListener;

    private static final int defaultNumChilds = 3;
    private SampleTab mCurTab;
    private int mPosition;
    protected float mExpanded;

    public SelectorTabLayout(Context context) {
        super(context);
        init(context, null);
    }

    public SelectorTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SelectorTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    void init(Context context, AttributeSet attrs) {

        for(int i=0; i<defaultNumChilds; i++) {
            SampleTab tab = new SelectorTabLayout.SampleTab(context);
            tab.setOnClickListener(this);
            tab.getMoreButton().setOnClickListener(this);
            tab.setRank(i);
            addView(tab);
        }
        // mExpanded represent the item weight which should be
        // equal to tabnum-1: 3 tabs give mExpanded = 2
        // It should be equal to the weight sum of the others non expanded tabs (weight = 1)
        // ex: non expanded(1 + 1) = 2 expanded.
        // That way expanded tab alway take half the allocated layout space
        mExpanded = (float) getChildCount()-1;
    }

    public void setOnSelectorTabActionListener(OnSelectorTabActionListener listener) {
        this.mListener = listener;
    }

    // Handle general tab click for selection or to open the popup menu
    @Override
    public void onClick(View view) {
        // option view come necessary from cur selected view

        SampleTab candidate = ((SampleTab)view);
        if (candidate != mCurTab || !candidate.isLoaded()) { // ignore if same view or it have been cleared
            // need to know tab position to handle result
             mPosition = indexOfChild(candidate);

            if (candidate.isLoaded()) {
                // First unselect and shrink curTab if set
                if (mCurTab != null) { // shrink if exist
                    mCurTab.setSelected(false);
                }
                // now curTab can safely be set to candidate
                mCurTab = candidate;
                mCurTab.setSelected(true);

            }
            // Now output selected tab somewhere
            if (mListener != null) {
//                Log.e(DEBUG_TAG,"onTabSelected(mPosition): " + mPosition);
                mListener.onTabSelected(mPosition);
            }
        }
    }

    // This method set the tab name and put it in selected state
    public void loadSample(String name) {
//        Log.e(DEBUG_TAG,"loadSample()");

        SampleTab candidate = (SampleTab) getChildAt(mPosition);
        // first reset last curTab to unselected before assigning candidate to curTab
        if (mCurTab != null) {
            mCurTab.setSelected(false);
        }
        mCurTab = candidate;
        mCurTab.setSelected(true);

        mCurTab.setSampleName(name);
    }

    // Clear the current tab and select the next loaded one on either left or right side
    public void clearSample() {

        SampleTab candidate = (SampleTab)getChildAt(mPosition);
        // we can only clear a loaded tab
        if (candidate.isLoaded()) candidate.setDefault();

        int index = mPosition;
        // set next left tab as the selected one
        // or if there is no more tab on left, do the same on the right side
        // as long they are not empty
        boolean found = false;
        while (!found && --index >= 0) {
            if (((SampleTab) getChildAt(index)).isLoaded()) {
                mCurTab = (SampleTab) getChildAt(index);
                mCurTab.setSelected(true);
                mPosition = index;
                found = true;
                if (mListener != null) {
//                    Log.e(DEBUG_TAG,"onTabSelected(mPosition): " + mPosition);
                    mListener.onTabSelected(mPosition);
                }
            }
        }
        while (!found && ++index<defaultNumChilds) {
            if (((SampleTab) getChildAt(index)).isLoaded()) {
                mCurTab = (SampleTab) getChildAt(index);
                mCurTab.setSelected(true);
                mPosition = index;
                found = true;
                if (mListener != null) {
//                    Log.e(DEBUG_TAG,"onTabSelected(mPosition): " + mPosition);
                    mListener.onTabSelected(mPosition);
                }
            }
        }
        if (!found) { // all cleared
            mPosition = 0;
//            Log.e(DEBUG_TAG,"onTabSelected(mPosition): All Cleared");
        }
    }


    // Menu item handler
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (mListener != null) {
//            Log.e(DEBUG_TAG,"onMenuItemClick(item): " + item + " position: " + mPosition);
            mListener.onTabPopupItemSelected(mPosition, item);
            return true;
        }
        return false;
    }



    ////////////////////////////////////////////////////////////////////////////////////////////
    // State saving handling and modules
    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        int pos = mPosition;
        boolean[] loaded = new boolean[defaultNumChilds];
        ArrayList<String> names = new ArrayList<>();
        boolean[] playing = new boolean[defaultNumChilds];
        int selected = -1;
        for (int i=0; i<defaultNumChilds; i++) {
            SampleTab t = (SampleTab) getChildAt(i);
            boolean isLoaded = t.isLoaded();
            loaded[i] = isLoaded;
            names.add(isLoaded ? t.mSampleName.getText().toString() : "");
            if (t.isSelected()) selected = i;
            playing[i] = isLoaded && t.mPlayButton.mChecked;
        }
        return new SavedState(superState, pos, loaded, names, playing, selected);
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        mPosition = savedState.pos;
        for (int i=0; i<defaultNumChilds; i++) {
            SampleTab t = (SampleTab) getChildAt(i);
            boolean loaded = savedState.loaded[i];
            if (loaded) {
                if (i == mPosition) {
                    loadSample((String) savedState.names.get(i));
                } else t.setSampleName((String) savedState.names.get(i));
                t.setSelected(i == savedState.selected);
                t.mPlayButton.setChecked(savedState.playing[i]);
            }
        }
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        // As we save our own instance state, ensure our children don't save and restore their state as well.
        super.dispatchFreezeSelfOnly(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        super.dispatchThawSelfOnly(container);
    }

    /**
     * Convenience class to save / restore the session tab state.
     */
    protected static class SavedState extends BaseSavedState {

        private final int pos;
        private final boolean[] loaded;
        private final ArrayList<String> names;
        private final int selected;
        private final boolean[] playing;

        private SavedState(Parcelable superState, int pos, boolean[] loaded, ArrayList<String> names,
                           boolean[] playing, int selected) {
            super(superState);
            this.pos = pos;
            this.loaded = loaded;
            this.names = names;
            this.selected = selected;
            this.playing = playing;
        }

        private SavedState(Parcel in) {
            super(in);
            pos = in.readInt();
            boolean[] load = new boolean[defaultNumChilds];
            in.readBooleanArray(load);
            loaded = load;
            names = in.readArrayList(String.class.getClassLoader());
            selected = in.readInt();
            boolean[] play = new boolean[defaultNumChilds];
            in.readBooleanArray(play);
            playing = play;
        }

        @Override
        public void writeToParcel(Parcel destination, int flags) {
            super.writeToParcel(destination, flags);
            destination.writeInt(pos);
            destination.writeBooleanArray(loaded);
            destination.writeStringList(names);
            destination.writeInt(selected);
            destination.writeBooleanArray(playing);
        }

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    public class SampleTab extends LinearLayoutCompat implements CheckButton.OnStateChangedListener {

        private static final String defaultTab = "+";

        private LayoutParams mParams;
        private View mPlayButtonFrame;
        private CheckButton mPlayButton;
        private View mMoreButtonFrame;
        private View mMoreButton;
        private TextView mSampleName;
        private boolean mLoaded = false;
        private int mRank;


        public SampleTab(Context context) {
            super(context);
            init(context);
        }

        public SampleTab(Context context, AttributeSet attrs) {
            super(context, attrs);
            init(context);
        }

        public SampleTab(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init(context);
        }


        void init(Context context) {
            LayoutInflater.from(context).inflate(R.layout.session_tab_view, this, true);
            mPlayButtonFrame = findViewById(R.id.play_button_frame);
            mPlayButton = findViewById(R.id.play_from_session_button);
            mMoreButtonFrame = findViewById(R.id.more_button_frame);
            mMoreButton = findViewById(R.id.options_button);
            mSampleName = findViewById(R.id.sample_name_text_view);
            mPlayButtonFrame.setVisibility(GONE);
            mMoreButtonFrame.setVisibility(GONE);
            mSampleName.setText(defaultTab);
            mParams = new LayoutParams(0,
                    LinearLayout.LayoutParams.MATCH_PARENT, 1);
            setLayoutParams(mParams);
            setSoundEffectsEnabled(false);
            mPlayButton.setOnStateChangedListener(this);
            mPlayButton.setSoundEffectsEnabled(false);
            mMoreButton.setSoundEffectsEnabled(false);
        }

        protected void setRank(int rank) { mRank = rank; }

        public View getMoreButton() {
            return (View) mMoreButton;
        }

        public CheckButton getPlayButton() { return mPlayButton; }

        public void setPlayButtonChecked(boolean b) { mPlayButton.setChecked(b); }

        @Override
        public void setSelected(boolean selected) {
            super.setSelected(selected);
            if (selected) {
                mPlayButtonFrame.setVisibility(VISIBLE);
                mMoreButtonFrame.setVisibility(VISIBLE);
                setWeight(mExpanded);
            } else {
                mMoreButtonFrame.setVisibility(GONE);
                setWeight(1);
            }
        }

        public void setSampleName(String name) {
            mPlayButtonFrame.setVisibility(VISIBLE);
            // weird; need to set to empty string before changing text
            // otherwise it seem to leave extra space
            mSampleName.setText("");
            mSampleName.setText(name);
            mLoaded = true;
        }

        public void setDefault() {
            mPlayButtonFrame.setVisibility(GONE);
            setSelected(false);
            mSampleName.setText(defaultTab);
            setWeight(1);
            mLoaded = false;
        }

        public boolean isLoaded() {
            return mLoaded;
        }

        public void setWeight(float weight) {
            mParams.weight = weight;
            setLayoutParams(mParams);
        }

        @Override
        public void onStateChanged(boolean state) {
            // redirect to parent SelectorTabLayout
            mListener.onTabPlayButtonClicked(mRank, state);
        }
    }

}
